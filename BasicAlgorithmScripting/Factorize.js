
function factorialize(num) {
   // Keep track of the result.
   var result = 1;
   // Keep track of the number being multiplied.
   var count = 1;
   // Simple loop to go through all of it;
   while(count < num) {
      result *= (count + 1);
      count++;
   }
   console.log(result);
}
factorialize(5);
