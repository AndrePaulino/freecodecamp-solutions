function titleCase(str) {
   // Split() setence into array of its words.
   var arr = str.toLowerCase().split(" ");
   // Loop through the array.
   for(var i in arr) {
      // Split() each word into a array of its letters.
      arr[i] = arr[i].split('');
      // toUpperCase() the first letter.
      arr[i][0] = arr[i][0].toUpperCase();
      // Join() the word back.
      arr[i] = arr[i].join('');
   }
   // Join() the new sentece.
   arr = arr.join(' ');
   console.log(arr);
}

titleCase("I am a little tea pot");
