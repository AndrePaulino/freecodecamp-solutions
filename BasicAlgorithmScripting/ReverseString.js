
function reverseString(str) {
   // Turn string into array where each charater is a element.
   var arr = str.split('');
   // Reverse that array.
   var reversedArr = arr.reverse();
   // Join() that array into a string.
   var reversedStr = reversedArr.join('');
   
   return reversedStr;
}
console.log(reverseString("hello"));

// Clean up
function reverse(string) {
   var reversed = string.split('').reverse().join('');
   console.log(reversed);
}
reverse('fafe');
