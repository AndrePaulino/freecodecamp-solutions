
function findLongestWord(str) {
   // Split() setence into array of its words.
   var words = str.split(' ');
   // Keep track of words length
   var longestWordLength = 0;
   // Loop through the words array and find the items length.
   for(var i in words) {
      if(words[i].length > longestWordLength) {
         longestWordLength = words[i].length;
      }
   }
   console.log(longestWordLength);
}
findLongestWord("The quick brown fox jumped over the lazy dog");
