
function largestOfFour(arr) {
   // Result array.
   var fourLargest = [];
      // First loop to go over the main array
      for(var i in arr) {
         // Important that the variable to track the largest number in the sub arrays stay inside the first loop, so every time iterates again resets to 0.
         var largest = 0;
         // Second loop to go over the sub arrays.
         for(var o in arr[i]) {
            // if statement to find the largest number.
            if(arr[i][o] > largest) {
               largest = arr[i][o];
            }
         }
         // Once found that number .push() it to the result array.
         fourLargest.push(largest);
      }
      console.log(fourLargest);
}

largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);
largestOfFour([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]);
