
function palindrome(str) {
   // Clean the string, romoving (/[\W_]/) all non alphanumerical charaters and .toLowerCase(), to avoid any errors if there's any upper case in the middle.
   var cleanStr = str.replace( /[\W_]/g, '').toLowerCase();
   // Reverse it
   var reversedStr = cleanStr.split('').reverse().join('');
   // Check if the reversed and original strings are equal, if so true, else false
   if(reversedStr === cleanStr) {
      return true;
   } else {
      return false;
   }
}
console.log(palindrome("RaCe  Car"));
